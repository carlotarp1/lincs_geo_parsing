import pandas as pd 


#JUST CHECKING QUALITY
ffile1 = open('../data/gen_data/ZSVC_2017_gct_col_info.tsv','r')
lines1 = [line1.strip().split('\t') for line1 in ffile1.readlines()]
ffile1.close()

fout = open('../data/gen_data/ZSVC_2017.gpr','w')
fout2 = open('../data/gen_data/ZSVC_2017_alldrugs_experiments.tsv','w')
fout2.writelines('id\tdrug_name\tcell_line\n')
#For DLRP: concentration converted to molar and time to h
i = 0
for l1 in lines1:
	if i != 0:
		if l1[10] == 'trt_cp':
			if l1[3] not in ['CD34','JURKAT']:
				fout.writelines(l1[0]+'\n')
				fout2.writelines(l1[0]+'\t'+l1[9]+'\t'+l1[3]+'\n')
	i+=1
fout2.close()
fout.close()
