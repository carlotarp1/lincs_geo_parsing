'''
This script contains examples for reading .gctx files in Python.
'''

import cmap.io.gct as gct
import cmap.io.plategrp as grp

# input files
path_to_gctx_file = '../Broad_LINCS_Level4_ZSVCINF_2017.gct'

# load data
GCTObject = gct.GCT(path_to_gctx_file)
GCTObject.read()

#Generate row info file
row_headers = GCTObject.get_rhd()
probes = GCTObject.get_row_meta('id')
gid = GCTObject.get_row_meta('pr_gene_id')
symbol = GCTObject.get_row_meta('pr_gene_symbol')
bing = GCTObject.get_row_meta('pr_is_bing')
landmark = GCTObject.get_row_meta('pr_is_lmark')

fout = open('../data/gen_data/ZSVC_2017_gct_row_info.tsv','w')
fout.writelines('id\tpr_gene_id\tpr_gene_symbol\tpr_is_bing\tpr_is_lmark\n')
for i in range(0,len(probes)-1):
	fout.writelines(probes[i]+'\t'+str(gid[i])+'\t'+str(symbol[i])+'\t'+str(bing[i])+'\t'+str(landmark[i])+'\n')
fout.close()



#Generate column info file
column_headers = GCTObject.get_chd()
plates = GCTObject.get_column_meta('det_plate')
wells = GCTObject.get_column_meta('det_well')
cls = GCTObject.get_column_meta('CL_Center_Specific_ID')
rns1 = GCTObject.get_column_meta('RN_Target_Gene_ID')
rns = GCTObject.get_column_meta('SM_Center_Compound_ID')
dose = GCTObject.get_column_meta('SM_Dose')
dose_u = GCTObject.get_column_meta('SM_Dose_Unit')
lincs = GCTObject.get_column_meta('SM_LINCS_ID')
name = GCTObject.get_column_meta('SM_Name')
p_t = GCTObject.get_column_meta('SM_Pert_Type')
time = GCTObject.get_column_meta('SM_Time')
time_u = GCTObject.get_column_meta('SM_Time_Unit')
gct_cols_id = GCTObject.get_column_meta('id')

fout = open('../data/gen_data/ZSVC_2017_gct_col_info.tsv','w')
fout.writelines('id\tdet_plate\tdet_well\tCL_Center_Specific_ID\tRN_Target_Gene_ID\tSM_Center_Compound_ID\tSM_Dose\tSM_Dose_Unit\tSM_LINCS_ID\tSM_Name\tSM_Pert_Type\tSM_Time\tSM_Time_Unit\n')
for i in range(0,len(plates)-1):
	fout.writelines(gct_cols_id[i]+'\t'+str(plates[i])+'\t'+str(wells[i])+'\t'+str(cls[i])+'\t'+str(rns1[i])+'\t'+str(rns[i])+'\t'+str(dose[i])+'\t'+str(dose_u[i])+'\t'+str(lincs[i])+'\t'+str(name[i])+'\t'+str(p_t[i])+'\t'+str(time[i])+'\t'+str(time_u[i])+'\n')
fout.close()
