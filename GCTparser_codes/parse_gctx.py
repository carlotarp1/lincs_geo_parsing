import cmap.io.gct as gct
import cmap.io.plategrp as grp
import pandas as pd

experiments = grp.read_grp('../data/gen_data/ZSVC.gpr')

GCTObject = gct.GCT('../Broad_LINCS_Level4_ZSVCINF.gct')
# GCTObject = gct.GCT('../Broad_LINCS_Level4_ZSVCINF_TRIMMED.gct')

GCTObject.read(cid=experiments)
	
# Get the experiment IDs
experiments = GCTObject.get_column_meta('id')

# Get the gene ids
pr_id = GCTObject.get_row_meta('pr_gene_symbol')
bing = GCTObject.get_row_meta('pr_is_bing')

# Rename column names by experiment ID
matrix_df = pd.DataFrame(GCTObject.matrix)

cols = matrix_df.columns.tolist()

i = 0
cols_rename = {}
for c in cols:
	cols_rename[c] = experiments[i]
	i+=1
matrix_df = matrix_df.rename(columns=cols_rename)

# Add column with gene symbol and bing 
matrix_df['pr_gene_symbol'] = pr_id
matrix_df['bing'] = bing
matrix_df['bing'] = matrix_df['bing'].apply(lambda x:int(x))
# # Filter out the probes where is_bing not 1 (quality filter)
#matrix_df = matrix_df[matrix_df['bing']==1]

matrix_df.to_csv('../data/gen_data/ZSVC_zscore_alldrugs.tsv',sep='\t',index=False)
