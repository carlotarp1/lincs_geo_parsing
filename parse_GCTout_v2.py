import csv
import lzma
import numpy as np
import pandas as pd


from tqdm import tqdm
from scipy.stats import rankdata
from os.path import exists
from collections import defaultdict


ZSCORE_FILE = 'data/gen_data/ZSVC_zscore_alldrugs.tsv'
ZSCORE_NO_DUPS = 'ZSCORE_notdupli.tsv.xz'
GENE_COLUMN = 115209
TOTAL_LINES = 22269
TOTAL_LINES_NODUPS = 9786

if not exists(ZSCORE_NO_DUPS):
    max_val = {}
    with open(ZSCORE_FILE, 'rt') as ffile:
        reader = csv.reader(ffile, delimiter='\t')
        for l, f in enumerate(tqdm(reader, total=TOTAL_LINES, desc="Computing medians".rjust(30))):
            if l == 0:
                continue

            gene = f[GENE_COLUMN]
            values_median = np.nanmedian([float(v) for v in f[:GENE_COLUMN]])
            if values_median > max_val.get(gene, (0, 0))[0]:
                max_val[gene] = (values_median, l)

    lines = set([v[1] for v in max_val.values()])
    lines.add(0)
    with lzma.open(ZSCORE_NO_DUPS, 'wt') as fout, open(ZSCORE_FILE, 'rt') as ffile:
        for l, line in enumerate(tqdm(ffile, total=TOTAL_LINES, desc="Writing {} lines".format(len(lines)).rjust(30))):
            if l in lines:
                fout.writelines(line)


columns = [(i, min(GENE_COLUMN, i+60000)) for i in range(0, GENE_COLUMN, 60000)]
for f_ini, f_end in columns:
    print("\n From {} to {}".format(f_ini, f_end))
    genes = []
    headers = []
    ranks = defaultdict(list)
    with lzma.open(ZSCORE_NO_DUPS, 'rt') as ffile:
        reader = csv.reader(ffile, delimiter='\t')
        for l, f in enumerate(tqdm(reader, total=TOTAL_LINES_NODUPS, desc="Loading columns".rjust(30))):
            if l == 0:
                headers = f[f_ini:f_end]
            else:
                genes.append(f[GENE_COLUMN])
                for c in range(f_ini, f_end):
                    ranks[c].append(float(f[c]))

    # Calculate ranks
    for k in tqdm(ranks, desc="Ranking".rjust(30)):
        ranks[k] = rankdata(ranks[k], method='dense').astype(np.uint16)

    print("Writing pandas dataframe")
    df = pd.DataFrame(
        index=headers,
        columns=genes,
        data=np.array([ranks[c] for c in range(f_ini, f_end)]),
        copy=False
    )
    df.to_pickle('ZSCORE_rank_{:06d}_{:06d}.dataframe.pickle'.format(f_ini, f_end))


