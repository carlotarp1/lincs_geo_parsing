#Steps to follow in order to generate the drug ranks file file

###########################################
# REQUIREMENTS                            #
###########################################
# Python 2.7                              #
# cmap library (included in the package)  #
# pandas                                  #
# numpy                                   #
# collections                             #
###########################################


1) Download ZSVC LINCS data (GCT file) from https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE70138

2) Run codes in GCTparser_codes/ to parse GCT file
	2.1) gen_gct_info.py
		Extracts all the information contained in the columns and rows of the GCT file including
		- Drug name for each experiment 
		- is_bing annotation for probes
	
		Stores data in two files, one for columns the other for rows:
		- ZSVC_gct_col_info.tsv
		- ZSVC_gct_row_info.tsv


	2.2) select_experiments_in_gct.py
		From all the experiments contained in lincs select those which are for cell lines which are not blood (I searched them manually: CD34 and JURKAT) and are for compounds (i.e exclude shRNA experimentS). 

		Stores the list of all experiments of interest:
		- ZSVC.gpr

	2.3) parse_gctx.py
		Extract zscore matrix for all experiments of interest, those found in the file created in 2.2 

		Stores the zscore matrix:
		- ZSVC_zscore_alldrugs.tsv

3) Select a single probe per gene and convert the Zscores to ranks. 
	3.1) parse_GCTout.py 

	- v2, faster version of v1. Step 4) woks with output of v1 currently.
		

4) Compute median rank per gene joining data of all drug experiments
	4.1) median_rank.py
		

