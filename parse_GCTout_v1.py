import pandas as pd
import numpy as np
import collections

#Do median of Zscores of experiments with probes mapping to same gene ID 
matrix = pd.read_csv('data/gen_data/gene_bing.txt',sep='\t',header=0,index_col=False)
# matrix = pd.read_csv('data/gen_data/ZSVC_zscore_alldrugs.tsv',sep='\t',header=0,index_col=False)

duplicated_genes = {item:count for item, count in collections.Counter(matrix.pr_gene_symbol.tolist()).items() if count > 1}
not_duplicated_genes = {item:count for item, count in collections.Counter(matrix.pr_gene_symbol.tolist()).items() if count == 1}
print('Duplicated genes:',len(duplicated_genes))
print('Not duplicated genes:',len(not_duplicated_genes))

max_val ={}
if len(duplicated_genes) > 0:
	for gene, num_dupis in duplicated_genes.items():
		print('Working on ',gene,', probes number:',num_dupis)
		ffile = open('data/gen_data/ZSVC_zscore_alldrugs.tsv','r')
		genes_f = 0
		l = 0
		max_val[gene] = [0,0]
		for line in ffile:
			if genes_f == num_dupis:
				break
			else:
				f = line.strip().split('\t')
				if l != 0: 
					print(gene,f[115209])
					if f[115209].startswith(gene[0]):
						if gene == f[115209]:
							values = [float(v) for v in f[:115208]]		
							if np.nanmedian(values) > max_val[gene][0]:
								max_val[gene] = [np.nanmedian(values),l]
							genes_f +=1	 
						else:
							continue
					else:
						continue
			l+=1
		ffile.close()

		print(gene,max_val)


print('Writting notdupli file')
fout = open('ZSCORE_notdupli_TEST.tsv','a')
ffile = open('data/gen_data/ZSVC_zscore_alldrugs.tsv','r')
l = 0
for line in ffile:
	f = line.strip().split('\t')
	if l != 0: 
		print(f[115209])
		if f[115209] in not_duplicated_genes:
			fout.writelines(line)
		else:
			if f[115209] in max_val:
				if l == max_val[f[115209]][1]:
					fout.writelines(line)
				else:
					continue
			else:
				continue
	else:
		fout.writelines(line)
		fout.flush()
	l+=1
ffile.close()
fout.close()

	
# ################################################################
# #Generate matrix with ranks, DLRP input
# newmatrix = pd.DataFrame()
# for col in matrix_complete.columns.tolist():
#	 if col != 'pr_gene_symbol' and col !='bing':
#		 vals = list(matrix_complete[col])
#		 sorted_vals = sorted(list(matrix_complete[col]))
#		 sorted_vals_index = [ sorted_vals.index(val)+1 for val in vals ]
#		 newmatrix[col] = sorted_vals_index

# newmatrix.to_csv('data/gen_data/ZSVC_ranks_alldrugs_notdupli.tsv',sep='\t',index=False)
