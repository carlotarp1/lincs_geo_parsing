
# coding: utf-8

# In[22]:

import pandas as pd 
import os 
import numpy as np
from scipy.stats import rankdata
import sys


#Load drug-experiment mapping
drugs_experiments = pd.read_csv('data/gen_data/ZSVC_gct_col_info.tsv',sep='\t',index_col=False,header=0,usecols=['id','SM_Name'])
# drugs_experiments = drugs_experiments[drugs_experiments['SM_Name']==sys.argv[1]]
drugs_experiments = drugs_experiments[drugs_experiments['SM_Name'].isin(["chelerythrine chloride","PF 573228","A 769662","RAF 265","calpain inhibitor ii","compound e","bafilomycin a1","okadaic acid","gsk j4"])]

# print('drug experiments loaded')
#Load selected experiments
experiments = pd.read_csv('data/gen_data/ZSVC.gpr',sep='\t',index_col=False,header=None,names=['id']).id.tolist()
# print('all experiments loaded')

#Filter drug-experiments for the selected experiments
drugs_experiments = drugs_experiments[drugs_experiments['id'].isin(experiments)]
# print('drug experiments filtered')


with open('data/gen_data/ZSVC_ranks_alldrugs_notdupli.tsv','r') as ffile:
    experiments_list = ffile.readline().strip().split('\t')

# print('ranking drug experiments')

# In[23]:

for drug, exps in drugs_experiments.groupby('SM_Name'):
    exps_l = exps.id.unique().tolist()
    exps_i = [experiments_list.index(e) for e in exps_l]

    # print('Wroking on',drug,': ',len(exps_i),' experiments')
    ffile = open('data/gen_data/ZSVC_ranks_alldrugs_notdupli.tsv','r')

    to_df = []
    for i, line in enumerate(ffile):

        if i == 0:
            continue

        f = line.strip().split('\t')
        if len(f) > 10:
            to_df.append({'GENE':f[115210],'MED_RANK':np.median([float(f[i]) for i in exps_i])})
    df = pd.DataFrame(to_df)
    df['RE_RANKED'] = rankdata(df['MED_RANK'], method='dense')
    
    df = df.rename(columns={'RE_RANKED':'RANK'})
    df['RANK'] = df['RANK'].apply(lambda x:int(x))
    df[['GENE','RANK']].to_csv('./ZSCV_drug_median_rank_v2/'+drug.replace(' ','_')+'.tsv',sep='\t',index=False)
    ffile.close()   




